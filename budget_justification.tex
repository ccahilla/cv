\documentclass[letterpaper,11pt]{article}

\usepackage{sidecap}
\usepackage{graphicx}
\usepackage[sorting=none]{biblatex}
\usepackage{latexsym}
\usepackage[empty]{fullpage}
\usepackage{titlesec}
\usepackage{marvosym}
\usepackage[usenames,dvipsnames]{color}
\usepackage{verbatim}
\usepackage{enumitem}
\usepackage[pdftex]{hyperref}
\usepackage{fancyhdr}
\usepackage{libertinus}
\usepackage[T1]{fontenc}
\hypersetup{
  colorlinks = true,
  urlcolor = [rgb]{0.1, 0.1, 0.54},
  citecolor = [rgb]{0.1, 0.54, 0.1},
  linkcolor = [rgb]{0.54, 0.1, 0.1}
}

\addbibresource{budget_justification.bib}

\pagestyle{fancy}
\fancyhf{} % clear all header and footer fields
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

% Adjust margins
\addtolength{\oddsidemargin}{0in}
\addtolength{\evensidemargin}{0in}
\addtolength{\textwidth}{0in}
\addtolength{\topmargin}{0in}
\addtolength{\textheight}{0in}

\urlstyle{same}

% \raggedbottom
% \raggedright
% \setlength{\tabcolsep}{0in}

% Sections formatting
% \titleformat{\section}{
%   \vspace{-4pt}\scshape\raggedright\large
% }{}{0em}{}[\color{black}\titlerule \vspace{-5pt}]

\makeindex
\begin{document}

\begin{flushright}
    \textbf{\Large Craig Cahillane}\\
    Syracuse University Startup Justification
%    \href{mailto:craigcahillane@fastmail.com}{craigcahillane@fastmail.com}\\
%    +1 202 9079680 \\
%    \href{https://ccahilla.github.io}{https://ccahilla.github.io/}\\
\end{flushright}


\noindent
The astrophysical sensitivity of a gravitational-wave observatory is limited by the level of noise in the detector. 
A detector's fundamental noise comes from a combination of three sources: seismic noise from ground motion at low frequencies, thermal motion of atoms in the mirrors and their coatings, and quantum uncertainty in the light circulating in the detector. 
Combining these three fundamental noise sources gives the detector's design sensitivity. 
However, \emph{no gravitational-wave detector has ever reached its design sensitivity at low frequencies.} 
The low-frequency sensitivity of Initial LIGO, Advanced LIGO, and Virgo are not limited by the detector's fundamental properties, but by a technical source of noise known as \emph{controls noise.} 
Mitigating this noise source is critical to extending the observatory's astrophysical reach, increasing the rate of detected events, and measuring the properties of detected signals.

The fundamental problem is illustrated in Figure.~\ref{fig:future_noise_curves_plus_controls_noise}. 
The green and red curves show the design sensitivity of the Advanced LIGO detector and its planned upgrade A$+$. 
The blue curve shows the best detector sensitivity achieved to date in the Advanced LIGO Hanford detector. 
As frequencies below 30~Hz, the detector's noise is dominated by the controls noise shown in orange. 
This noise is preventing LIGO from accessing an entire one and a half octaves of the gravitational-wave spectrum between 10 Hz and 30 Hz. 

Control noise scales linearly with the length of the detector arms. 
The light orange curve shows the predicted scaling of the current control noise in Cosmic Explorer, compared to the scaling of the the fundamental noise sources (purple curve). 
If we do not address the origin of controls noise in Cosmic Explorer, controls noise will render all improvements in fundamental noise lower than $60~\mathrm{Hz}$ irrelevant and hide a decade of the gravitational-wave spectrum from us. 
The stated science goals for Cosmic Explorer include the detection of gravitational waves from the first black hole mergers in the distant universe.
This is impossible without drastic improvement to the controls problem.
\textbf{The purpose of my laboratory will be to solve the problem of controls noise for Advanced LIGO, A$+$ and Cosmic Explorer.}
\begin{SCfigure}[][h]
    \caption{ 
        Detector gravitational wave strain sensitivity curves for current and future detectors.
        Each curve represents a measured or projected noise level, with lower noise at a given frequency corresponding to a more sensitive detector.
    }     
    \includegraphics[width=0.7\textwidth]{./figures/future_detector_noise_curves/future_detector_noise_curves_plus_controls_noise.pdf}
    \label{fig:future_noise_curves_plus_controls_noise}
\end{SCfigure}  

\noindent
Controls noise is the noise associated with the many control loops used to hold the detector's core optics at their ultra-precise positions and angles. Mitigation of controls noise is challenging because of the balancing act the control loop must play between having very high suppression of noise versus imposing sensing noise on the mirror.
In general, the control loops configuration are chosen to have very high gain to reduce mirror motion down to acceptable levels to avoid throwing the interferometer out of operation. However, in this high-gain configuration, noise from the sensors is mistaken as true displacement of the mirrors, and this noise is actually imposed on the mirrors.

There are many avenues to combating controls noise that we are currently pursuing.  
The first and foremost involve actual commissioning projects at the detector now, 
including design of digital filters for optimal controls in the presence of high laser power, development of robust measurement techniques, diagonalization of sensors and controller matricies,
sensor feedforward techniques, and multiple-input multiple-output controls modeling. 
Data-based controls projects include inference projects, including estimation of hard-to-know interferometer parameters, like resonant arm powers, 
losses in the interferometer, and beam spots. 
% These parameters are critical for the correct calibration of gravitational-wave data. 
Other avenues include nonlinear controls investigations, including implementing adaptive techniques that can ``learn'' mirror positions outside the linear regime, 
deterministic locking of multiple degrees of freedom at once, and subtraction of bilinear angular noise from length controls. 
Finally, development of better sensors for mirror motion detection is a huge priority for future detectors.

While primary controls noise mitigation must ultimately be implemented at the LIGO observatories, it is not possible to perform active research and development on these detectors while they are frozen for an astrophysical observing run. 
During this period, my group will require an experimental setup where controls noise mitigation techniques can be prototyped in Syracuse, then deployed at the observatories when ready. 
Beyond the need to test and develop mitigation techniques for Advanced LIGO, A$+$, Cosmic Explorer, and the proposed Voyager upgrade to LIGO, present a new range of challenges. 
These detectors may operate with two micron lasers, rather than the one micron systems presently used. 
New detector sensors will be needed if this technology is selected as the path forward.

My experimental setup will be built so that it can be upgraded to address the challenges of A+ and, ultimately, Cosmic Explorer. 
I will build a high-finesse suspended cavity operating with a 2~Watt 1064~nm laser, with data acquisition and control systems that can be used to explore the various noise sources. 
This system will replicate the systems used at the LIGO observatories, allowing my students to gain practical knowledge of LIGO's infrastructure. Data acquisition systems with low noise and fast sampling rates are necessary for high-quality control loops. 
The lab must be a cleanroom, with a positive pressure gradient to push dust out of the experiment setting. 
Optics must be high quality, with the correct shape and reflective coating, and sensors must be low-noise. 
Environmental seismometers are required to monitor the seismic motion experiences by the cavity suspensions. 
The cavity must be in vacuum, as residual gas noise, which comes from atoms in the atmosphere bouncing off our mirrors, would otherwise destroy the sensitivity of the experiment. 
To mitigate electronics noise, which comes from the component circuits and photodiodes that sense the optical signal, much of the electronics will require low-noise in-house circuit design, assembly, and testing. 
Laser safety gear is essential to keep lab personnel safe during laser operation.

Once the initial 2~Watt 1064~nm system is producing results, I will submit a proposal to the National Science Foundation's Major Research Infrastructure (MRI) solicitation to upgrade the lab to a 55~watt facility. 
Experiments to mitigate controls noise for A+ and Cosmic Explorer will require a cavity that has both high finesse and high power. 
This will be a development, rather than acquisition, MRI proposal as I will need to upgrade several aspects of the initial lab setup. 
A 55~Watt facility would also be useful to Ballmer and Mansell, so this can be proposed to NSF as a shared facility at Syracuse.

Third-generation detectors are currently in the design stage, with the recent publication of the Cosmic Explorer Horizon Study.  
Research and development for mission-critical technology must be funded now and in the immediate future for the success of Cosmic Explorer. 
I will submit funding proposals both to the NSF Support of LIGO Research program (for improvement to current detectors) and as part of the Cosmic Explorer Major Research Facilities and Construction Conceptual Design program. 
Progress in controls noise will be challenging, but the are a number of avenues along which progress can be made, as described above. 
Experimental results will inform and advance R\&D for both LIGO and Cosmic Explorer. 
%Investing in lab infrastructure now, and generating  papers demonstrating relevant experimental results, will engender confidence in the lab and create grant opportunities from the NSF and others interested in low-noise, high-power optomechanics.

% \printbibliography

\end{document}
