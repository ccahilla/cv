# Craig Cahillane's Curriculum Vitae

Email: craigcahillane@fastmail.com

[Curriculum Vitae](https://gitlab.com/ccahilla/cv/-/jobs/artifacts/master/file/cv.pdf?job=compile_pdf)

[Cover Letter](https://gitlab.com/ccahilla/cv/-/jobs/artifacts/master/file/cover_letter.pdf?job=compile_pdf)

[Research Statement](https://gitlab.com/ccahilla/cv/-/jobs/artifacts/master/file/research_statement.pdf?job=compile_pdf)

[Publication List](https://gitlab.com/ccahilla/cv/-/jobs/artifacts/master/file/publication_list.pdf?job=compile_pdf)

[Budget Justification](https://gitlab.com/ccahilla/cv/-/jobs/artifacts/master/file/budget_justification.pdf?job=compile_pdf)

# Webpage

[Craig Cahillane github page - https://ccahilla.github.io/](https://ccahilla.github.io/)

# LIGO Outreach

[How does LIGO detect gravitational waves? youtube video submitted to SoME1 competition](https://www.youtube.com/watch?v=X7RJHxeCulY)

[Interactive Fabry-Perot optical cavity](https://ccahilla.github.io/fabryperot.html)

[Black Hole Merger Demonstration](https://ccahilla.github.io/blackholedemo.html)

[Interactive LIGO Hanford Noisebudget](https://ccahilla.github.io/lho_noisebudget.svg)