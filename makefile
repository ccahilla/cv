# LaTeX Makefile
DATE=date+"%m%d%Y"
PREFIX=/Library/TeX/texbin/
FILE=cv
all: $(FILE).pdf

.PHONY: clean

clean:
	rm -rf *.blg
	rm -rf *.out
	rm -rf *.bbl
	rm -rf *.log
	rm -rf *.ind
	rm -rf *.ilg
	rm -rf *.lot
	rm -rf *.lof
	rm -rf *.ind
	rm -rf *.idx
	rm -rf *.aux
	rm -rf *.toc
	rm -rf *.fls
	rm -rf *.xml
	rm -rf *.bcf
	rm -rf *.fdb_latexmk	
	rm -rf *.synctex.gz
	rm -f *.pdf


$(FILE).pdf: *.tex *.bib
	latexmk -pdf $(FILE).tex
	latexmk -pdf cover_letter.tex
	latexmk -pdf publication_list.tex
	latexmk -pdf research_statement.tex
