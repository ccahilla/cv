'''
Code to generate plots for the Dec 18 MIT GRITTS Talk

Craig Cahillane
Dec 16, 2019
'''

import os
import numpy as np
import h5py
import gwinc

import scipy.constants as scc
import scipy.special as scp

import nds2utils as nu
from nds2utils.make_interactive_svg import make_interactive_svg

# For the waveforms
# import pylab
from pycbc import waveform

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=2)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

data_dir = f'{script_dir}/data'

plot_names = np.array([])


#####   Functions   #####
def SNR(fff, hf, Shh):
    ''' Calculates SNR from equation (16) of https://arxiv.org/pdf/1408.0740.pdf
    rho^2 = Integral from 0 to inf of 4*|hf|^2/Shh(f) * df
    rho = SNR
    fff = frequency vector in Hz
    hf  = strain amplitude in strain/Hz
    Shh = strain noise PSD in strain**2/Hz

    Returns rho = SNR
    '''
    assert len(fff) == len(hf), 'SNR function frequency vector length does not equal the GW strain vector length'
    assert len(fff) == len(Shh), 'SNR function frequency vector length does not equal the strain noise PSD length'
    if fff[0] == 0: # if the first data point is 0, remove it
        fff = fff[1:]
        hf = hf[1:]
        Shh = Shh[1:]

    integrand = 4 * hf**2 / Shh
    rho2 = np.trapz(integrand, x=fff)
    return np.sqrt(rho2)

def M_chirp(m1, m2):
    ''' Chirp mass of a gravitational wave merger'''
    return (m1 * m2)**(3/5) / (m1 + m2)**(1/5)

def M_ratio(m1, m2):
    ''' nu = symmetric mass ratio of a GW merger'''
    return m1 * m2 / (m1 + m2)**2


#####   Parameters   #####
L = 3994.5 # m
c = scc.c
M_sun = 1.989e30 # kg
Mpc = 3.086e22 # m



# GW150914 params
m1_0 = 36.0
m2_0 = 29.0
M_chirp_0 = 30.7 * M_sun # kg
nu_0 = 0.247
t_c = 2.50e-3 # s, coalescence time
phi_c = 0.600 # rad, coalescence phase
dist_eff_0 = 756 * Mpc # m, depends on the actual system distance and the orientation of the source 


# PhenomD waveforms:
# https://journals.aps.org/prd/pdf/10.1103/PhysRevD.93.044007
# Appendix B

# # Get a time domain waveform
# hp, hc = waveform.get_td_waveform(
#             approximant="IMRPhenomB", 
#             mass1=m1_0, 
#             mass2=m2_0, 
#             delta_t=1.0/4096, 
#             f_lower=20,
#             inclination=0,
#             distance=690
#             )
# tt_GW150914 = hp.sample_times.data
# td_data_GW150914 = hp.data

# Get a frequency domain waveform
sptilde, sctilde = waveform.get_fd_waveform(approximant="IMRPhenomD", 
                    mass1=m1_0, 
                    mass2=m2_0, 
                    delta_f=0.125, 
                    f_lower=0.1,
                    inclination=0,
                    distance=690)

ff_GW150914 = sptilde.sample_frequencies.data
darm_GW150914 = np.abs(sptilde.data)
char_darm_GW150914 = 2 * np.abs(sptilde.data) * np.sqrt(ff_GW150914) # strain / rtHz

fflog = np.logspace(np.log10(1), np.log10(7000), 500)

#####   O1 DARM   #####
data_O1 = np.loadtxt(f'{data_dir}/2015_10_24_15_09_43_H1_O1_strain.txt')
ff_O1 = data_O1[:,0]
asd_O1 = data_O1[:,1] # strain/rtHz
logff_O1, logasd_O1 = nu.linear_log_ASD(fflog, ff_O1, asd_O1)

#####   O3 DARM   #####
# retrieve once, save as .pkl
# load as .pkl if already saved
darm = 'H1:CAL-DELTAL_EXTERNAL_DQ' # cts
omc = 'H1:OMC-DCPD_SUM_OUT_DQ' # mA
pkl_file = f'{data_dir}/O3_darm_data.pkl'
if not os.path.exists(pkl_file):
    channels = [darm, omc]
    gps_start = 1258348788
    gps_stop  = 1258349288
    duration = 500 #s
    binwidth = 0.125 #Hz
    overlap = 0.5
    darmDict = nu.get_CSDs(channels, gps_start, gps_stop, binwidth, overlap)
    nu.save_pickle(darmDict, pkl_file)
else:
    darmDict = nu.load_pickle(pkl_file)

zeros = 30 * np.ones(6)
poles = 0.3 * np.ones(6)
gain = 1 # nu0/L
units = 'm' # 'Hz'
darmDict = nu.calibrate_chan(darmDict, darm, zeros, poles, gain=gain, units=units)

units = 'mA'
darmDict = nu.calibrate_chan(darmDict, omc, [], [], gain=1.0, units=units)

omcff = darmDict[omc]['ff']
omcASD = darmDict[omc]['calASD'] # mA/rtHz
darmff = darmDict[darm]['ff']
darmASD = darmDict[darm]['calASD'] # m/rtHz
darmASD = darmASD / L # strain/rtHz

idxs = nu.bandlimit_applier(darmff, darmff[1], 7000)
omcff  = omcff[idxs]
omcASD = omcASD[idxs]
darmff  = darmff[idxs]
darmASD = darmASD[idxs]

omc_logff, omc_logASD = nu.linear_log_ASD(fflog, omcff, omcASD)
darm_logff, darm_logASD = nu.linear_log_ASD(fflog, darmff, darmASD)

# aLIGO design 
budget = gwinc.load_budget('aLIGO', freq=fflog)
trace = budget.run()
aLIGO_design_ASD = trace.asd

# Budget, ifo, freq_, plot_style = gwinc.load_ifo('aLIGO')
# ifo = gwinc.precompIFO(fflog, ifo)
# traces = Budget(fflog, ifo=ifo).calc_trace()
# aLIGO_design_ASD = L * np.sqrt(traces['Total'][0])

# A+ design 
budget = gwinc.load_budget('Aplus', freq=fflog)
trace = budget.run()
Aplus_ASD = trace.asd

# Budget, ifo, freq_, plot_style = gwinc.load_ifo('Aplus')
# ifo = gwinc.precompIFO(fflog, ifo)
# traces = Budget(fflog, ifo=ifo).calc_trace()
# Aplus_ASD = L * np.sqrt(traces['Total'][0])

# Voyager design 
budget = gwinc.load_budget('Voyager', freq=fflog)
trace = budget.run()
Voyager_ASD = trace.asd

# CE1 design
budget = gwinc.load_budget('CE1', freq=fflog)
trace = budget.run()
CE1_ASD = trace.asd

# Budget, ifo, freq_, plot_style = gwinc.load_ifo('CE1')
# ifo = gwinc.precompIFO(fflog, ifo)
# traces = Budget(fflog, ifo=ifo).calc_trace()
# CE1_ASD = L * np.sqrt(traces['Total'][0])

# CE2 design
budget = gwinc.load_budget('CE2silicon', freq=fflog)
trace = budget.run()
CE2_ASD = trace.asd

# Budget, ifo, freq_, plot_style = gwinc.load_ifo('CE2')
# ifo = gwinc.precompIFO(fflog, ifo)
# traces = Budget(fflog, ifo=ifo).calc_trace()
# CE2_ASD = L * np.sqrt(traces['Total'][0])

SNR_dict = {}
keys = ['O1', 'O3', 'aLIGO', 'Aplus', 'Voyager', 'CE1', 'CE2silicon']
for key in keys:
    print(key)
    if key == 'O1':
        temp_ff_GW150914 = ff_GW150914[80:]
        temp_darm_GW150914 = darm_GW150914[80:]
        temp_ff = ff_O1[:len(temp_ff_GW150914)]
        temp_psd = ( asd_O1[:len(temp_ff_GW150914)] )**2
    elif key == 'O3':
        temp_darm_GW150914 = darm_GW150914[1:]
        temp_ff = ff_GW150914[1:]
        temp_asd = darmDict[darm]['calASD'][1:] / L
        temp_psd = ( temp_asd[:len(temp_ff)] )**2
    else:
        temp_darm_GW150914 = darm_GW150914[1:]
        temp_ff = ff_GW150914[1:]

        budget = gwinc.load_budget(key, freq=temp_ff)
        trace = budget.run()
        temp_psd = trace.psd

        # Budget, ifo, freq_, plot_style = gwinc.load_ifo(key)
        # ifo = gwinc.precompIFO(temp_ff, ifo)
        # traces = Budget(temp_ff, ifo=ifo).calc_trace()
        # temp_psd = L**2 * traces['Total'][0]

    SNR_dict[key] = SNR(temp_ff, temp_darm_GW150914, temp_psd)
for key in keys:
    print(f'{key} SNR = {SNR_dict[key]:.0f}')
for key in keys:
    print('{} SNR/O1 SNR Ratio= {:.2f}'.format(key, SNR_dict[key]/SNR_dict['O1']))


# Read in the O4 noisebudget
hf = h5py.File(f'{data_dir}/lho_darm_noisebudget.hdf5', 'r')

# This is the NB frequency vector
o4_freq = hf['Freq'][:]

# This is DARM PSD, the main red trace, in m^2/Hz
o4_darm_psd = hf['H1']['budget']['DARMMeasured']['PSD'][:] / L**2 # strain^2 / Hz

asc_psd = hf['H1']['budget']['ASC']['PSD'][:] / L**2 # strain^2 / Hz
lsc_psd = hf['H1']['budget']['LSC']['PSD'][:] / L**2 # strain^2 / Hz

controls_psd = asc_psd + lsc_psd

o4_darm_asd = np.sqrt(o4_darm_psd)
controls_asd = np.sqrt(controls_psd)

# CE2 design
budget = gwinc.load_budget('CE2silicon', freq=o4_freq)
trace = budget.run()
ce_asd = trace.asd
ce_psd = ce_asd**2

ce_plus_controls_psd = controls_psd/100.0 + ce_psd

ce_plus_controls_asd = np.sqrt(ce_plus_controls_psd)

savedata = np.vstack((o4_freq, ce_plus_controls_asd)).T
savefile = f'{data_dir}/ce2_plus_controls_asd.txt'
np.savetxt(savefile, savedata, header='Frequency [Hz], CE plus aLIGO controls ASD [strain/rtHz]')

# Plotting frequency vector 
# fflog = np.logspace(np.log10(1), np.log10(7000), 500)

# # PCAL full strength
# nu0 = scc.c/1064e-9 # Hz
# L = 3994.5 # m
# def PCAL(fff):
#     PCAL_max_at_100_Hz = 1e-16 # m/rtHz
#     PCAL_Hz = PCAL_max_at_100_Hz * (fff/100.0)**(-2) #* nu0/L # m/rtHz
#     return PCAL_Hz

# # ESD full strength
# nu0 = scc.c/1064e-9 # Hz
# L = 3994.5 # m
# def ESD(fff):
#     ESD_max_at_100_Hz = 1e-15 # m/rtHz
#     ESD_Hz = ESD_max_at_100_Hz * (fff/100.0)**(-2) #* nu0/L *# m/rtHz
#     return ESD_Hz

# # DARM
# darm = 'H1:CAL-DELTAL_EXTERNAL_DQ'
# channels = [darm]
# gps_start = 1258348788
# gps_stop  = 1258349288
# duration = 500 #s
# binwidth = 0.1 #Hz
# overlap = 0.5
# darmDict = nu.get_PSDs(channels, gps_start, gps_stop, binwidth, overlap)

# zeros = 30 * np.ones(6)
# poles = 0.3 * np.ones(6)
# gain = 1.0 #nu0/L
# units = 'm'
# darmDict = nu.calibrate_chan(darmDict, darm, zeros, poles, gain=gain, units=units)

# darmff = darmDict[darm]['ff']
# darmASD = darmDict[darm]['calASD'] / L   # strain / rtHz
# idxs = nu.bandlimit_applier(darmff, darmff[1], 7000)
# darmff  = darmff[idxs]
# darmASD = darmASD[idxs]
# darm_logff, darm_logASD = nu.linear_log_ASD(fflog, darmff, darmASD)

# # ALS DIFF 
# import dtt2hdf
# dttFile = '/Users/ccahilla/Git/IFO/ALS/data/ALSCOMM_and_DIFF_Performance.xml'
# dtt = dtt2hdf.read_diaggui(dttFile)
# res = dtt['results']['PSD']['H1:ALS-C_DIFF_PLL_CTRL_OUT_DQ']
# ffdiff = res['FHz'][1:]
# ASDdiff = res['PSD'][0,1:].reshape(np.shape(ffdiff))*1e-6 # Put from um/rtHz into m/rtHz units
# #ASDdiff = ASDdiff * nu0/L * 2 # Put from m/rtHz to Hz/rtHz, recall that nu_g = nu0 * 2

# diff_logff, diff_logASD = nu.linear_log_ASD(fflog, ffdiff, ASDdiff)

# # Single green FP shot noise 
# lam_g = 1064e-9/2.0 # m
# nu_g = scc.c/lam_g # Hz

# Pin_g = 1.0 # W, input green power
# LL = 3994.5 # m
# FSR = scc.c/(2 * LL)
# tEX_g = np.sqrt(0.079)
# tEY_g =	np.sqrt(0.079)
# tIX_g = np.sqrt(0.0096)
# tIY_g = np.sqrt(0.011)

# rEX_g = np.sqrt(1.0 - tEX_g**2)
# rEY_g = np.sqrt(1.0 - tEY_g**2)
# rIX_g = np.sqrt(1.0 - tIX_g**2)
# rIY_g = np.sqrt(1.0 - tIY_g**2)

# rArmX_g = (-rEX_g + rIX_g) / (1.0 - rEX_g * rIX_g)
# rArmY_g = (-rEY_g + rIY_g) / (1.0 - rEY_g * rIY_g)
# tArmX_g = tEX_g * tIX_g / (1.0 - rEX_g * rIX_g)
# tArmY_g = tEY_g * tIY_g / (1.0 - rEY_g * rIY_g)

# PreflX_g = Pin_g * rArmX_g**2
# PreflY_g = Pin_g * rArmY_g**2
# PtransX_g = Pin_g * tArmX_g**2
# PtransY_g = Pin_g * tArmY_g**2
# PcircX_g = Pin_g * (tEX_g / (1.0 - rEX_g * rIX_g))**2
# PcircY_g = Pin_g * (tEY_g / (1.0 - rEY_g * rIY_g))**2

# armPoleX_g = FSR/(2*np.pi) * np.log( 1.0/(rEX_g * rIX_g) )
# armPoleY_g = FSR/(2*np.pi) * np.log( 1.0/(rEY_g * rIY_g) )

# finesseX_g = FSR/(2 * armPoleX_g)
# finesseY_g = FSR/(2 * armPoleY_g)

# # PDH 
# modFreq = 24.9e6 # Hz
# modDepth = 0.03 # rad, guess
# visibility = 0.99 # guess

# opticalGainX = scc.c * 4 * Pin_g * scp.j0(modDepth) * scp.j1(modDepth) /(LL * lam_g * armPoleX_g * (1 + 1j*fflog/armPoleX_g)) # W/m
# opticalGainY = scc.c * 4 * Pin_g * scp.j0(modDepth) * scp.j1(modDepth) /(LL * lam_g * armPoleY_g * (1 + 1j*fflog/armPoleY_g)) # W/m

# # X arm shot noise
# shotNoiseX = np.sqrt(4 * nu_g * scc.Planck * Pin_g * ((1 - visibility) * scp.j0(modDepth)**2  + 3 * scp.j1(modDepth)**2)) # W/rtHz
# calShotNoiseX = np.abs( shotNoiseX / opticalGainX ) #* nu_g/LL # Hz/rtHz

# # Low noise VCO 
# # alog 11385 https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=11385
# data_vco = np.loadtxt(f'{data_dir}/VCO_FDD_frequency_noise_140415.txt')
# cal_vco = L/(nu0/2)
# vco_ff = data_vco[:,0]
# vco_asd = data_vco[:,1] * cal_vco # Hz/rtHz * m/Hz

# vco_logff, vco_logASD = nu.linear_log_ASD(fflog[:380], vco_ff, vco_asd) # stop at 900 Hz

#####   Plots   #####

### Time domain GW150914 waveform 
# fig, (s1) = plt.subplots(1)

# s1.plot(tt_GW150914, td_data_GW150914, label='GW150914')

# s1.set_xlim([-0.2, 0.03])

# s1.set_xlabel('Time [s]')
# s1.set_ylabel('Strain')

# s1.grid()
# s1.grid(which='minor', ls='--')
# s1.legend()


# plot_name = f'time_domain_gw150914_waveform.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



# ### OMCDCPD vs DARM 
# fig, (s1) = plt.subplots(1)

# s1.loglog(omc_logff, omc_logASD, label='OMC DCPD sum '+r'[$\mathrm{mA}/\sqrt{\mathrm{Hz}}$]')
# s1.loglog(darm_logff, darm_logASD, label='O3 DARM '+r'[$\mathrm{m}/\sqrt{\mathrm{Hz}}$]')

# s1.set_xlabel('Frequency [Hz]')
# s1.set_ylabel('ASDs [$\mathrm{?}/\sqrt{\mathrm{Hz}}$]')

# s1.set_xlim([10, 7000])
# s1.set_ylim([1e-20, 1e-4])

# s1.grid()
# s1.grid(which='minor', ls='--')
# s1.legend()


# plot_name = f'omcdcpd_vs_darm.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()



### GW150914 SNR Comparison with future detectors
fig, (s1) = plt.subplots(1)

o1_idx_low = np.argwhere(logff_O1 > 10)[0,0] 
o1_idx_high = np.argwhere(logff_O1 > 6000)[0,0] 

# s1.loglog(ff_GW150914, char_darm_GW150914, color='k', 
#                                     label='GW150914 Characteristic Strain '+r'$2 |h(f)| \sqrt{f}$')
# s1.loglog(logff_O1[o1_idx_low:o1_idx_high], logasd_O1[o1_idx_low:o1_idx_high],     
#                                     label='O1 DARM (LHO)')
# s1.loglog(darm_logff, darm_logASD,  label='O3 DARM (LHO)')
s1.loglog(o4_freq, o4_darm_asd,             label='Advanced LIGO Pre-O4 Hanford measured noise')
s1.loglog(o4_freq, controls_asd,            label='Advanced LIGO controls noise')
s1.loglog(o4_freq, controls_asd/10.0,   color='C1', alpha=0.5, label='Advanced LIGO controls noise / 10 ')
s1.loglog(fflog, aLIGO_design_ASD,          label='Advanced LIGO design')
s1.loglog(fflog, Aplus_ASD,                 label='Advanced LIGO design with A+ upgrades')
# s1.loglog(fflog, Voyager_ASD,             label='Voyager design')
# s1.loglog(fflog, CE1_ASD,                 label='CE1 design')
s1.loglog(fflog, CE2_ASD,                   label='Cosmic Explorer design')
# s1.loglog(o4_freq, ce_plus_controls_asd,    label='CE plus aLIGO controls noise')

s1.set_xlabel('Frequency [Hz]')
s1.set_ylabel('Strain ASD [$1/\sqrt{\mathrm{Hz}}$]')

s1.set_xlim([3, 7000])
s1.set_ylim([1e-25, 1e-20])

s1.grid()
s1.grid(which='minor', ls='--')
s1.legend(loc='upper right')


plot_name = f'future_detector_noise_curves_plus_controls_noise.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()


# ### DARM vs DIFF 
# fig = plt.figure()
# s1 = fig.add_subplot(111)

# # PCAL Full strength
# s1.loglog(fflog, PCAL(fflog), 
#             label='PCAL Full Strength')

# # ESD Full strength
# s1.loglog(fflog, ESD(fflog), 
#             label='Low-noise ESD Full Strength')

# # Single FP arm green shot noise
# s1.loglog(fflog, calShotNoiseX,
#             label='Green arm shot noise')

# # DARM
# s1.loglog(darm_logff, darm_logASD, 
#             label='DARM')

# # ALS DIFF
# s1.loglog(diff_logff, diff_logASD,
#             label='ALS DIFF')

# # Low-noise VCO
# s1.loglog(vco_logff, vco_logASD,
#             label='Low-noise VCO')

# s1.set_xlabel('Frequency [Hz]')
# s1.set_ylabel('Displacement ASD [$\mathrm{m}/\sqrt{\mathrm{Hz}}$]')

# s1.set_xlim([1, fflog[-1]])
# s1.set_ylim([1e-20, 1.01e-11])

# s1.set_yticks(nu.good_ticks(s1))

# s1.grid()
# s1.grid(which='minor', ls='--', alpha=0.6)
# s1.legend()


# plot_name = f'darm_vs_diff.pdf'
# full_plot_name = '{}/{}'.format(fig_dir, plot_name)
# plot_names = np.append(plot_names, full_plot_name)
# print('Writing plot PDF to {}'.format(full_plot_name))
# plt.savefig(full_plot_name, bbox_inches='tight')
# plt.close()


# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()