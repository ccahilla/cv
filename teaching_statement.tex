\documentclass[letterpaper,11pt]{article}

\usepackage[sorting=none]{biblatex}
\usepackage{latexsym}
\usepackage[empty]{fullpage}
\usepackage{titlesec}
\usepackage{marvosym}
\usepackage[usenames,dvipsnames]{color}
\usepackage{verbatim}
\usepackage{enumitem}
\usepackage[pdftex]{hyperref}
\usepackage{fancyhdr}
\usepackage{libertinus}
\usepackage[T1]{fontenc}
\hypersetup{
  colorlinks = true,
  urlcolor = [rgb]{0.1, 0.1, 0.54},
  citecolor = [rgb]{0.1, 0.54, 0.1}
}

\addbibresource{research_statement.bib}

\pagestyle{fancy}
\fancyhf{} % clear all header and footer fields
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

% Adjust margins
\addtolength{\oddsidemargin}{0in}
\addtolength{\evensidemargin}{0in}
\addtolength{\textwidth}{0in}
\addtolength{\topmargin}{0in}
\addtolength{\textheight}{0in}

\urlstyle{same}

% \raggedbottom
% \raggedright
% \setlength{\tabcolsep}{0in}

% Sections formatting
% \titleformat{\section}{
%   \vspace{-4pt}\scshape\raggedright\large
% }{}{0em}{}[\color{black}\titlerule \vspace{-5pt}]

\makeindex
\begin{document}

\begin{flushright}
  \textbf{\Large Craig Cahillane}\\
  \href{mailto:craigcahillane@fastmail.com}{craigcahillane@fastmail.com}\\
  +1 202 9079680 \\
  \href{https://ccahilla.github.io}{https://ccahilla.github.io/}\\
\end{flushright}
\vspace{5mm}

% Introduction
My career in physics has shown me that very quickly, you can become responsible for specialized knowledge. 
Science communication, data visualization, and education of students and peers is crucial to building widespread knowledge and honing your ideas.
Good communication reduces duplicated effort and allows scientists to work together rather than in isolation.
% Additionally, we must expand our repertoire of communication tools to allow the student to learn in a wide variety of ways.

% Undergrad TAs
As an undergrad at Notre Dame, I was a teaching assistant my junior and senior years for Modern Physics, Mechanics, and Theory of Computing.
I held recitations and office hours for the physics students, and graded about one hundred Theory of Computing homework assignments a week.
As a TA, the first thing you learn is you did not learn everything possible from the class you took before.
I learned to think on my feet about the student's question, trying to create a discussion with them to reach the answer.

% SURF Mentoring
As a grad student at Caltech, I mentored two Summer Undergraduate Research Fellows (SURF) students. 
For our SURF projects, I started with the groundbreaking research idea to interest the student and motivate their work,
then work backwards to find where the student's knowledge lies.  
From there, we can give the student their first science task that they can accomplish, no matter their current capability.

All students come in with different levels of expertise, and different gaps in their understanding.
In the first week, my students needed help learning how to plot data and how to code in python.
From there, we pursued the mathematical background required to reasonably achieve their science goals.
Then we applied their skills to a simplified version of the ultimate project.

During the ten week SURF programs, the student is highly reliant on input from the mentor to determine the direction of the project.
On longer timescales, I want to cultivate self-reliance and confidence within the student on the direction research should take.
Confidence comes with expertise and familiarity with an research problem, and the best way to gain those is early exposure to difficult problems.

The experience of mentoring SURF students helped me realize my skillset, and explore the best way to share those skills with talented, motivated students.
I believe that the research experience my students had brought them closer to an understanding of what research is, 
and gave them a strong background in modern tech like \texttt{git}, data visualization, and statistics they can bring to any field.

% LSC Fellow Mentoring
In addition to mentoring SURF students, as a senior grad student I mentored LSC Fellows at LIGO Hanford in a more unofficial but still important capacity.
The experience of sitting in the control room at LIGO Hanford for the first time can be overwhelming.
The tools of a detector commissioner are not always user-friendly, and finding the information you need can be difficult.
An experienced hand in taking measurements on the actual detector is a practical necessity.  

I was able to assist several LSC Fellows in understanding how the detector works,
and guide them toward successfully gathering the data they needed for their projects.
In some cases I sat down and worked with them through the math of control loops, 
or how to read a circuit diagram, 
or debug bad control loop measurement and model comparisons.

% Outreach
As a grad student at Caltech I was the data analysis group outreach lead for three years.
I traveled to dozens of elementary schools and high schools in the LA area, 
bringing demonstrations to help visualize gravity like the Spandex Universe featuring a gravity well,
a diode laser michelson interferometer showing interference fringes,
and a movie of a binary black hole merger.
I added a new demo to the spandex universe, the \href{https://ccahilla.github.io/blackholedemo.html}{Black Hole Drill}, 
which produced waves that could be visualized with the naked eye when aliased with a strobe light.
Also, I created an \href{https://ccahilla.github.io/fabryperot.html}{interactive Fabry-Perot optical cavity} in javascript,
and would bring a laptop to demos and let people try to resonant laser light in the cavity.

When talking to elementary school children up to high schoolers and parents, your tactics to get the listener interested must change.  
For children we focused on asking them questions about the demos, 
mostly trying to draw the connection between the planets, the Sun, and the abstract term ``gravity'' which most kids have heard but not understood.
For older children and parents we tried to emphasize that gravity is curvature in spacetime, black holes are just extremely curved spacetime,
and LIGO could detect black holes in deep space using powerful lasers on the earth.

Most recently, I made a \href{https://www.youtube.com/watch?v=X7RJHxeCulY}{``math explainer'' youtube video about how LIGO detects gravitational waves}.
This was made for the Summer of Math Exposition 1 (SoME1) competition using a python animation library \texttt{manim}.
I envision my youtube channel expanding to include more ``math explainer''-type videos, particularly for potentially confusing concepts in LIGO 
like quantum squeezing, shot noise, coupled cavities, Pound-Drever-Hall locking, and more.

% Tech
Part of teaching is teaching your experienced colleagues how to use the code or technology you've developed.
In my case, I've helped developed code for quickly making new feedforward filters,
and the \href{https://noisebudget.docs.ligo.org/aligoNB/}{online interactive noise budget},
that must be used by other commissioners on a regular basis.
To facilitate code reusability, I document my code via docstrings and READMEs, 
save all code in git repositories,
and save a working python3 anaconda virtual environments to each git repo, with instructions, so others down the line can easily use my work.

% An extremely important aspect of this work is user-friendliness, 
% because many users need to be able to make changes, updates, and add new plots for the noise budget.
% Additionally, we want the updates to be immediately visible to the collaboration.
% I have used git and continuous integration (gitlab CI) to display the work of commissioners at LIGO Hanford in real time
% in a way that's easily consumable by anyone in the collaboration.

% Teaching aspirations
Although I lack experience with lecture, I believe I have the skills necessary to assemble a successful physics course, 
with classic strategies like lectures, worked examples, and office hours, 
as well as harnessing new tech like animations of physics concepts or interactive web pages. 
My expertise is in lasers, optomechanics, and control loops, with strong understanding in electrical engineering and computer science.
I have a grad student level of understanding in electrodynamics and quantum mechanics, 
with extra knowledge in the realm of quantum optics and quadrature squeezing.
Finally, I have curiosity and am willing to learn deeply about a subject to present it cleanly.

% Conclusions


\end{document}