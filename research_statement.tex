%-----------------------------------
% Craig Cahillane Research Statement
% December 10, 2019

\documentclass[letterpaper,11pt]{article}

\usepackage[sorting=none]{biblatex}
\usepackage{latexsym}
\usepackage[empty]{fullpage}
\usepackage{titlesec}
\usepackage{marvosym}
\usepackage[usenames,dvipsnames]{color}
\usepackage{verbatim}
\usepackage{enumitem}
\usepackage[pdftex]{hyperref}
\usepackage{fancyhdr}
\usepackage{libertinus}
\usepackage[T1]{fontenc}
\hypersetup{
  colorlinks = true,
  urlcolor = [rgb]{0.1, 0.1, 0.54},
  citecolor = [rgb]{0.1, 0.54, 0.1}
}

\addbibresource{research_statement.bib}

\pagestyle{fancy}
\fancyhf{} % clear all header and footer fields
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

% Adjust margins
\addtolength{\oddsidemargin}{0in}
\addtolength{\evensidemargin}{0in}
\addtolength{\textwidth}{0in}
\addtolength{\topmargin}{0in}
\addtolength{\textheight}{0in}

\urlstyle{same}

% \raggedbottom
% \raggedright
% \setlength{\tabcolsep}{0in}

% Sections formatting
% \titleformat{\section}{
%   \vspace{-4pt}\scshape\raggedright\large
% }{}{0em}{}[\color{black}\titlerule \vspace{-5pt}]

\makeindex
\begin{document}

\begin{flushright}
  \textbf{\Large Craig Cahillane}\\
  \href{mailto:craigcahillane@fastmail.com}{craigcahillane@fastmail.com}\\
  +1 202 9079680 \\
  \href{https://ccahilla.github.io}{https://ccahilla.github.io/}\\
\end{flushright}
\vspace{5mm}

%%% Introduction
In 2015, the second-generation of the Laser Interferometer Gravitational-wave Observatory, called Advanced LIGO, first achieved astrophysical sensitivity.
The project was the successor to the first-generation initial LIGO detectors, which ran from 2002 to 2010 and made no detections of gravitational waves.

Just prior to its first observing run, Advanced LIGO made the first-ever detection of gravitational waves from a binary black hole merger \cite{PhysRevLett.116.061102}.
During the second observing run, Advanced LIGO and Virgo detected gravitational waves from a binary neutron star merger \cite{PhysRevLett.119.161101}. 
This detection was coincident with a gamma-ray burst detected by the \textit{Fermi} Gamma-ray Burst Monitor at the same sky-location, 
marking the first ever joint gravitational-wave and electromagnetic observation of an astrophysical event \cite{Goldstein_2017}.
From 2015 to 2020, around 50 other compact binary system mergers have been detected \cite{GWTC1, GWTC2}. 
The field of experimental gravity finally has regular direct observations of the most powerful events in the universe.

From these era-defining detections, the fields of astronomy, astrophysics, general relativity, and cosmology have reaped huge benefits that otherwise would have been impossible,
including new knowledge about the rates of binary formation, and the formation of our galaxies \cite{RatesPaper2016, RatesPaper2019},
direct measurements of black hole properties like mass, spin, and sky location imprinted on the gravitational wave signals \cite{GW150914PEPaper}
neutron star radii and equation of state measurements \cite{GW170817neutronstarequationofstate},
direct tests of general relativity using signals from extremely high-curvature, high-energy events \cite{GW150915GRTests, O3aGRtests2019},
the detection of higher-order multipoles of gravitational waves from a neutron star black hole merger ringdowns \cite{potentialNSBH},
the standard siren method of measuring the Hubble constant from binary neutron star detectors \cite{BNS_standard_siren, Chen2018}, 
neutron star black hole sirens \cite{PhysRevLett.121.021303}, 
or the so-called ``dark sirens'' method \cite{dark_sirens}.

Now, in an era where new astrophysical gravitational wave data is rapidly driving science forward,
improving the quality and quantity of the data is of paramount importance.
The Advanced LIGO detectors are currently sensitive to gravitational waves from binary neutron star mergers out to 110 megaparsecs,
and are undergoing A+ upgrades to further improve this sensitivity.
Additionally, plans for the third-generation of gravitational-wave detectors, such as Cosmic Explorer and Einstein Telescope, 
are being developed based on the knowledge gathered from the current-gen detectors \cite{CosmicExplorerHorizonStudy, EinsteinTelescope2011}.

As a postdoc at LIGO Hanford Observatory, I am involved in making the A+ upgrades a reality.
Before as a grad student I was an LIGO Science Collaboration Fellow who spent a year at the detector commissioning leading up to observing run three (O3).
After O3 began, I was a member of the Advanced LIGO O3 detector sensitivity four-person paper writing team \cite{O3_commissioning_paper}.

As a detector scientist, my duties involve commissioning the detector to bring it online in a robust and timely manner,
as well as characterize and improve the performance of the detector.
While at Hanford I have developed and implemented a new method of directly measuring the arm power using radiation pressure,
measured and modeled the noise in the laser frequency stabilization servo and intensity stabilization servo,
measured and modeled the DARM plant optical spring and compared to theory to estimate losses within the signal recycling cavity,
modeled the power budget to estimate the new low arm losses and proposed a new optimal power-recycling mirror transmission,
revamped the Advanced LIGO noise budget to incorporate automatic commit integration,
verified the performance of the low-loss output Faraday isolator,
tested and repaired the common mode circuit boards,
and simulated the interferometer in Finesse to estimate frequency noise coupling to DARM.

Earlier in my graduate student career, I created the Advanced LIGO calibration uncertainty pipeline \cite{PhysRevD.96.102001}.
The purpose of this pipeline was to characterize the uncertainty of LIGO's final product: astrophysical strain data.
The uncertainty in the strain data had usually been an afterthought, until the first detection.
Then the scientific value of the data skyrocketed, as did the uncertainty in that data.
In 2015, I produced the uncertainty budget for the first binary black hole detection data, GW150914 \cite{PhysRevD.95.062003}.
Later on, I would revamp the pipeline to incorporate some more advanced statistical tools, 
including Markov Chain Monte Carlo methods to fit detector model parameters,
and Gaussian Process Regressions to catch unmodeled deviations in the measured detector response.
The Gaussian Process Regression proved useful for describing the overall bias due to the DARM optical plant anti-spring effect, 
which was observed but not modeled in the earlier observing runs.
The same calibration uncertainty pipeline is still in place today \cite{Sun2020}.

Halfway through my PhD, I changed from the calibration group to join the Coatings Thermal Noise lab at Caltech.
Here I began hands-on optics and electronics work.
The Coatings Thermal Noise lab was dedicated to measuring the Brownian motion associated with aluminum gallium arsinide (AlGaAs) coatings.
AlGaAs coatings are a candidate low-noise optical coating for the core optics of second- and third-generation detectors.
The experiment consisted of two in-vacuum fixed-spacer Fabry-P\'erot cavities, with two input lasers locked to each cavity.
The Brownian motion of each cavity was imprinted on the laser frequency. 
The transmitted beams were then combined together with a beatnote of around 100~MHz to detect the Brownian noise imprinted on the laser.
In this lab I learned the fundamentals of table-top optics, seismic isolation, laser frequency and intensity stabilization, 
Pound-Drever-Hall locking, photodetection, electronics fabrication, and noise analysis.  

Now, as a postdoc with unique knowledge of the current Advanced LIGO detectors,
I am working to bring new ideas to tackle current commissioning problems,
as well as harness our knowledge to design future detectors.
One example is my latest paper, ``Laser Frequency Noise in Next-Generation Gravitational-Wave Detectors'' \cite{3gfn_paper}.
For Cosmic Explorer and Einstein Telescope, the longer arms of the detectors lead to difficulties that will render the current frequency stabilization system obsolete.
From our measurements of the laser frequency stabilization servo and frequency noise coupling to the gravitational wave channel,
we created a frequency noise requirement for Cosmic Explorer,
and proposed input optics necessary to satisfy that requirement.

With my years of experience in data analysis, table-top experiment, and actually running Advanced LIGO, 
I am well-situated to continue measuring and modeling current detectors to both improve current detectors to achieve design sensitivity,
as well as launch research and development projects for future detectors.
Here I will describe overall project themes I am working on for current and future detectors, 
and dive in the specifics of each one.

\textbf{Improved controls for A+ and beyond}\\
Currently, the biggest limitation for the performance of Advanced LIGO is excess motion at low frequencies, especially between 10 and 60~Hz.
This motion is caused by our control systems, which are required to hold the interferometer in its resonant regime.
This noise is known as \textit{controls noise}. 

Controls noise is extremely difficult to mitigate for a number of reasons.
The first reason is the relatively large amount of motion the controls must suppress to achieve GW sensitivity. 
This requires high bandwidth, high-gain controllers.
The second reason is the quantum limit of our sensors is not low enough to achieve GW sensitivity. 
requiring low bandwidth controllers to avoid imposing sensing noise our on loops.
This leads to tradeoffs between high-noise, robust controllers and low-noise, fragile controllers.
Additionally, there are measured high levels of cross-coupling between these control loops, including angular control loops to the DARM loop.

At this stage in commissioning multiple-input multiple-output (MIMO) controls modeling is crucial.
% Excess motion from controls is a serious problem that ultimately limits gravitational wave sensitivity at low frequency.
% Cross-coupling of control loops only exacerbates this problem.
We have measured at LIGO Hanford the effect of the arm angular controls on the DARM plant at low frequency.

Currently, we use feedforward and offline noise subtraction techniques to try and remove known measurement noises from DARM directly.
However, this cannot remove second-order noise effects, such as nonlinearity or upconversion.
The best way to remove such noise effects is to reduce overall motion.
To do so robustly, MIMO controls modeling is the first step, with comparison to measurement necessary to verify our models against the many unknowns (spot positions, losses, etc).
Our MIMO model can then be used to help diagonalize our controllers, and reduce cross-couplings in our loops.

This project will require ongoing input from commissioners, similar to the \href{https://noisebudget.docs.ligo.org/aligoNB/}{online noise budget}.
There are many subprojects related to this work, including 
analysis of multiple coherence between loops,
analysis of nonlinear noise, especially in angular loops,
development of long, tuned noise injections for low-coherence, high-fidelity measurements of angular loops without losing lock,
simulation of the Hanford interferometer and control loop response together using \href{https://github.com/kevinkuns/qlance}{qlance}, 
the Quantum optomechanics Loop Analysis and Noise Calculation Engine written by Kevin Kuns,
and incorporation of my own median-averaging cross-spectral density and transfer function code to remove glitches from long-term measurements.

Some of these subprojects are appropriate for a grad student to start working with LIGO data and learn the basics of control loop analysis.
Additionally, students could travel to the sites as LSC Fellows and perform some measurements themselves.


\textbf{Interferometer inference}.\\ 
LIGO pushes the envelope when it comes to precision measurement and system purity.
Very tiny disturbances or system impurity can spoil LIGO's sensitivity.
This includes scattered or retroreflected photons into the main beam, 
beam jitter from vibrating optics mounts,
radiation pressure from the high power beam itself,
or mode mismatch between cavities.

Each of these impurities will leave distinct signatures on the interferometer output.
In some cases its obvious and easy to measure, like in the case of DARM optical spring.
The DARM optical antispring was first measured at LIGO Hanford in observing run one, 
and is caused by the signal-recycling cavity being detuned from anti-resonance.
As the DARM calibration modeler for creating the uncertainty budget, 
this was my first foray into what I now call interferometer inference, 
which is the process of estimating detector parameters from actual measurements.

As a grad student, I extended this concept to measure the resonating laser power in the arms and the loss in the signal recycling cavity.
I believe there is much untapped knowledge in this sort of inference measurement using measured transfer functions from various output ports of the interferometer.
Not all parameters are obvious or have easy ways to measure them, like mode matching.
Tapping into this knowledge requires good detector models and a strong understanding of the controls system.  
The next step is to identify parameters we want to know with precision for future detectors like Einstein Telescope and Cosmic Explorer,
and think carefully about which transfer functions we can use to isolate those parameters as much as possible,
and consider potential pitfalls in the measurement technique.
Such measurements can be performed in simulation first, like in Finesse, or modeled analytically from first principles, then tested on a real interferometer. 
This project lends itself well to up-and-coming students of precision interferometry.


\textbf{Design of future detectors}
Research and development for building a successful third-generation gravitational wave detector will be an critical aspect of all work in experimental gravity.
I am in the process of publishing our findings for the laser frequency noise requirements for Cosmic Explorer-like interferometers \cite{3gfn_paper}.

There are two avenues of pursuing the success of future detectors today: simulation and lab verification.
Simulation is important for finding what key interferometric parameter values must be to achieve design sensitivity for Cosmic Explorer.
Key parameters include loss in various parts of the interferometer, mode matching, and noise requirements.

Lab verification is trying to measure key parameters in the lab, either directly or indirectly and scaling to Cosmic Explorer scales.
One potential lab verification project for me is measurement of frequency noise from an input optics setup like the one proposed in \cite{3gfn_paper}.

The dawn of gravitational wave physics is behind us.
The nature of ground-based laser interferometer detectors makes it possible to rapidly expand our astrophysical range to the cosmological scale.
A host of known unknowns awaits the construction of third-generation detectors, making new science on the oldest objects in our universe possible. 


\printbibliography

\end{document}